package com.example.springdemo.ex1;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Scanner;

@Configuration
public class Communicator {

    private NewMessage nm;
    private SmartMessage sm;

    public Communicator(NewMessage nm, SmartMessage sm) {
        this.nm = nm;
        this.sm = sm;
    }

//    @Scheduled(fixedDelay = 10000)
    public void every10seconds() {
        nm.newMessage();
        Scanner scanner = new Scanner(System.in);
        sm.smartMessage(scanner.nextLine());
    }
}

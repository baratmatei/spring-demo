package com.example.springdemo.ex1;

import org.springframework.stereotype.Component;

@Component
public class SmartMessage {
    public void smartMessage(String message){
        System.out.println(">> Hi "+ message + ", I still have amnesia. i'll ask again in 5 seconds");
    }
}

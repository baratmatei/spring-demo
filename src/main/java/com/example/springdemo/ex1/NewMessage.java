package com.example.springdemo.ex1;

import org.springframework.stereotype.Component;

@Component
public class NewMessage {
    public void newMessage(){
        System.out.print(">> Hi, I'm the first Spring demo and I have amnesia. Who are you?\n<<");
    }
}

package com.example.springdemo.jpa.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity(name="teachers")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Teacher {
    @Id
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer salary;
    private Integer subjectId;
}

package com.example.springdemo.jpa.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Classes")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class StudentClass {
    @Id
    private Integer id;
    private String className;
}

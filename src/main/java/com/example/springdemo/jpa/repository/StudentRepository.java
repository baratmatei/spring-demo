package com.example.springdemo.jpa.repository;

import com.example.springdemo.jpa.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

//annotation for spring to create a bean of this "interface"
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    //JpaRepository -> holds functionality for data management

    List<Student> findByFirstName (String name);
    List<Student> findByFirstNameAndLastName(String firstName, String lastName);
    List<Student> findByClassId (Integer cls);
    List<Student> findByDateOfBirth(LocalDate dateOfBirth);

}

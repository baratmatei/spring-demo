package com.example.springdemo.jpa.repository;

import com.example.springdemo.jpa.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
    List<Teacher> findBySubjectId(Integer subjectId);

}

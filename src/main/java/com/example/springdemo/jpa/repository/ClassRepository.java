package com.example.springdemo.jpa.repository;

import com.example.springdemo.jpa.entity.StudentClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepository extends JpaRepository<StudentClass, Integer> {
}

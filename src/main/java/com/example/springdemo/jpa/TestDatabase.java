package com.example.springdemo.jpa;

import com.example.springdemo.jpa.entity.Student;
import com.example.springdemo.jpa.repository.StudentRepository;
import com.example.springdemo.jpa.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
public class TestDatabase {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Scheduled(fixedDelay = 10000)
    public void testDataBase() {
//        List<Student> studentList = studentRepository.findAll();
//        studentList.forEach(student -> System.out.println("Student id " + student.getId() + ": " + student.getFirstName() + " " + student.getLastName()));
//        System.out.println("--------------------------------------------");

        printMathTeachers();
        printAllStudentsInAClass();
        printStudentsBornOnDate();

//        List<Student> studentsByFirstName = studentRepository.findByFirstName("Vasile");
//        System.out.println("Students with the first name 'Vasile':");
//        studentsByFirstName.forEach(student -> System.out.println("Student id " + student.getId() + ": " + student.getFirstName() + " " + student.getLastName()));
//        System.out.println("--------------------------------------------");
//
//        List<Student> studentsByFirstNameAndLastName = studentRepository.findByFirstNameAndLastName("Marcel", "Grigorescu");
//        System.out.println("Students with the first name 'Marcel' and last name 'Grigorescu':");
//        studentsByFirstNameAndLastName.forEach(student -> System.out.println("Student id " + student.getId() + ": " + student.getFirstName() + " " + student.getLastName()));
//        System.out.println("--------------------------------------------");
    }

    private void printStudentsBornOnDate() {
        System.out.println("All students born on 12/08/2000: " + studentRepository.findByDateOfBirth(LocalDate.parse("2000-08-12")));
    }

    private void printAllStudentsInAClass() {
        System.out.println("Number of students in class 12B: " + studentRepository.findByClassId(1).size());
    }

    private void printMathTeachers() {
        System.out.println("All math teachers: " + teacherRepository.findBySubjectId(1));
    }
}

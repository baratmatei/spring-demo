package com.example.springdemo.beans;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class PrettyCommunicator {
    public void prettyPrint(String message){
        System.out.println("---------------------------------------------------");
        System.out.println("| " + message + " |");
        System.out.println("---------------------------------------------------");
    }
}

package com.example.springdemo.beans;

import org.springframework.stereotype.Component;

@Component
public class NeutralPrinter {

    public void neutralPrint(String message){
        System.out.println("whatever " + message);
    }
}

package com.example.springdemo.beans;

import org.springframework.stereotype.Component;

@Component
public class UglyPrinter {

    public void uglyPrint(String message){
        System.out.println("ugly " + message);
    }
}

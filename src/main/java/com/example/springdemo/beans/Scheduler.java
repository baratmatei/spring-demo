package com.example.springdemo.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class Scheduler {
    @Autowired
    PrettyCommunicator pc;

    private UglyPrinter up;

    private NeutralPrinter np;

    public Scheduler (UglyPrinter up) {
        this.up = up;
    }

    @Autowired
    public void setNp(NeutralPrinter np) {
        this.np = np;
    }

    //fixed delay is in seconds
//    @Scheduled(fixedDelay = 5000)
//    public void every5seconds() {
//        pc.prettyPrint("Message every 5 seconds");
//    }
//    @Scheduled(fixedDelay = 3000)
//    public void every3seconds() {
//        up.uglyPrint("Message every 3 seconds");
//    }
//    @Scheduled(fixedDelay = 3000)
//    public void every7seconds() {
//        np.neutralPrint("Message every 3 seconds");
//    }


}

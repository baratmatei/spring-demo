package com.example.springdemo.advancedBeans;

import org.springframework.context.annotation.Configuration;

@Configuration
public interface GenericReader {
    String readLine();
}

package com.example.springdemo.advancedBeans;

import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class ReadFromFile implements GenericReader {
    BufferedReader br;

    public ReadFromFile() {
        try {
            br = new BufferedReader(new FileReader(new File("src/main/resources/read.txt")));

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    @Override
    public String readLine() {
        try {
            return br.readLine();
        } catch (IOException e) {
            System.out.println("Could not read");
        }
        return null;
    }
}

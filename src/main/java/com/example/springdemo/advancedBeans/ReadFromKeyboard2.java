package com.example.springdemo.advancedBeans;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Primary
@Component
public class ReadFromKeyboard2 implements GenericReader{
    Scanner scanner = new Scanner(System.in);
    @Override
    public String readLine() {
        return "<<-----" + scanner.nextLine();
    }
}

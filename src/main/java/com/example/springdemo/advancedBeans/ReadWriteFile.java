package com.example.springdemo.advancedBeans;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.*;

@Configuration
public class ReadWriteFile {

    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void fileReadWriteExample() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/read.txt"));
            System.out.println(reader.readLine());
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

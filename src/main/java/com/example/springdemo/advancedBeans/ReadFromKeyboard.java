package com.example.springdemo.advancedBeans;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ReadFromKeyboard implements GenericReader{
    Scanner scanner = new Scanner(System.in);
    @Override
    public String readLine() {
        System.out.print("<<");
        return scanner.nextLine();
    }
}

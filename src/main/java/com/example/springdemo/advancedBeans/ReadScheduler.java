package com.example.springdemo.advancedBeans;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;

@Configuration
public class ReadScheduler {

    ReadFromFile rff;
    ReadFromKeyboard rfk;

    @Autowired
    GenericReader genericReader;

    public ReadScheduler(ReadFromFile rff, ReadFromKeyboard rfk) {
        this.rff = rff;
        this.rfk = rfk;
    }

    @Scheduled(fixedDelay = 3000)
    public void readLine(){
//        System.out.println(rff.readLine());
//        System.out.println(rfk.readLine());
//        System.out.println(genericReader.readLine());
    }
}
